﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game_2048
{
    public partial class FormGame : Form
    {
        GameEngine gengine;
        public FormGame()
        {
            InitializeComponent();

            Graphics dpiGraphics = Graphics.FromHwnd(IntPtr.Zero);
            this.AutoScaleDimensions = new SizeF(dpiGraphics.DpiX, dpiGraphics.DpiX);
            this.AutoScaleMode = AutoScaleMode.Dpi;
            dpiGraphics.Dispose();

            gengine = new GameEngine();
        }

        private void pictureBoxField_Paint(object sender, PaintEventArgs e)
        {
            gengine.Draw(e.Graphics, pictureBoxField.Width, pictureBoxField.Height);
        }

        private void FormGame_KeyDown(object sender, KeyEventArgs e)
        {
            bool CallRefresh = false;
            if (e.KeyCode == Keys.Right)
            {
                gengine.OnKeyDown(Direction.Right);
                CallRefresh = true;
            }
            else if (e.KeyCode == Keys.Left)
            {
                gengine.OnKeyDown(Direction.Left);
                CallRefresh = true;
            }
            else if (e.KeyCode == Keys.Up)
            {
                gengine.OnKeyDown(Direction.Up);
                CallRefresh = true;
            }
            else if (e.KeyCode == Keys.Down)
            {
                gengine.OnKeyDown(Direction.Down);
                CallRefresh = true;
            }
            else if (gengine.Over && e.KeyCode == Keys.R)
            {
                gengine.Restart();
                CallRefresh = true;
            }
            if (CallRefresh)
            {
                pictureBoxField.Refresh();
            }
            GC.Collect();
        }
    }
}
